
var gulp            = require('gulp'),
    sass            = require('gulp-ruby-sass'),
    pngquant        = require('imagemin-pngquant'),
    sequence        = require('run-sequence'),
    del             = require('del'),
    yesno           = require('yesno'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    plugins         = gulpLoadPlugins({
                        pattern: ['gulp-*', 'gulp.*'],
                        replaceString: /^gulp(-|\.)/,
                        rename: {
                          'gulp-concat-sourcemap'    : 'concatSourcemap',
                          'gulp-merge-media-queries' : 'mmq',
                          'gulp-clean-css'           : 'cleanCSS',
                          'gulp-cache-bust'          : 'cachebust'
                        }
                      });


// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// GLOBS
var paths = {

  dev_base    : 'dev/',
  dev_html    : 'dev/html/',
  dev_scss    : 'dev/scss/',
  dev_ts      : 'dev/ts/',
  dev_js      : 'dev/js/',
  dev_img     : 'dev/imgs/',
  dev_vid     : 'dev/video/',
  dev_fonts   : 'dev/type/',
  dev_config  : 'dev/config/',
  dev_fallback: 'dev/fallback/',
  dev_actions : 'dev/action/',

  prod_base    : './www/',
  prod_css     : './www/css/',
  prod_js      : './www/js/',
  prod_img     : './www/img/',
  prod_vid     : './www/video/',
  prod_fonts   : './www/type/',
  prod_fallback: './www/fallback/',
  prod_actions : './www/action/'
};
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //




// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
var isBuild      = false,
    mmq_log      = true,
    action       = 'dev',
    releaseCheck = [],
    tasks        = [
                    'html',
                    'scss',
                    'js',
                    'img',
                    'webp',
                    'type',
                    'config'
                  ];



if ( plugins.util.env.build === true ) {
  // --build flag

  isBuild      = true;
  mmq_log      = false;
  action       = 'build';

} else if ( plugins.util.env.release === true ) {
  // --release flag

  isBuild      = true;
  mmq_log      = false;
  action       = 'release';
  releaseCheck = ['bump'];

}
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //






// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
var pkg = require('./package.json');
// Print this to the scss and js on build
var banner = [
  '',
  ' ',
  '/**',
  ' * Version: <%= pkg.version %>',
  ' * Developer',
  ' * <%= pkg.author %> [black-marked.com]',
  ' * Copyright (c) 2016 black-marked',
  ' * All rights reserved',
  '**/',
  ' ',
  ''
].join('\n');
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//  .d8888b.  8888888888 88888888888 888     888 8888888b.
// d88P  Y88b 888            888     888     888 888   Y88b
// Y88b.      888            888     888     888 888    888
//  "Y888b.   8888888        888     888     888 888   d88P
//     "Y88b. 888            888     888     888 8888888P"
//       "888 888            888     888     888 888
// Y88b  d88P 888            888     Y88b. .d88P 888
//  "Y8888P"  8888888888     888      "Y88888P"  888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


gulp.task('del', releaseCheck , function (cb) {

  console.log(
    ' ' + '\n' +
    ' Clean Started'
  );

  plugins.cached.caches = {};
  del( paths.prod_base);
  cb();
});



gulp.task('setup', ['del'], function (cb) {

  return gulp.src('*.js', {read: false})
    .pipe(plugins.shell( 'bundler install'))
    .on( 'end', function() { plugins.util.log( plugins.util.colors.blue( '\n' + '\n' + 'Environment setup' + '\n') ); }, cb);
});

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //




// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 888    888 88888888888 888b     d888 888
// 888    888     888     8888b   d8888 888
// 888    888     888     88888b.d88888 888
// 8888888888     888     888Y88888P888 888
// 888    888     888     888 Y888P 888 888
// 888    888     888     888  Y8P  888 888
// 888    888     888     888   "   888 888
// 888    888     888     888       888 88888888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //



var paths_html = {

  html: [
    paths.dev_html + '*.html',
    paths.dev_html + '**/*.html',
  ]

};

gulp.task('html', function (cb) {

  console.log(
    ' ' + '\n' +
    'HTML Started'
  );

  return gulp.src(paths_html.html)
    .pipe(isBuild ? plugins.util.noop() : plugins.print())
    .pipe(isBuild ? plugins.util.noop() : plugins.cached('html-cache'))
    .pipe(plugins.htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(isBuild ? plugins.cachebust() :  plugins.util.noop())
    .pipe(gulp.dest(paths.prod_base))
    .pipe(plugins.livereload(), cb);

});

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //






// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//  .d8888b.   .d8888b.   .d8888b.   .d8888b.
// d88P  Y88b d88P  Y88b d88P  Y88b d88P  Y88b
// Y88b.      888    888 Y88b.      Y88b.
//  "Y888b.   888         "Y888b.    "Y888b.
//     "Y88b. 888            "Y88b.     "Y88b.
//       "888 888    888       "888       "888
// Y88b  d88P Y88b  d88P Y88b  d88P Y88b  d88P
//  "Y8888P"   "Y8888P"   "Y8888P"   "Y8888P"
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


var paths_scss = {

  scss_master: paths.dev_scss + 'zensation.scss',

  scss_watch: [
    paths.dev_scss + '*.scss',
    paths.dev_scss + '**/*.scss'
  ],

  css_master: paths.prod_css + 'zensation.css'

};



gulp.task('scss', function (cb) {

  console.log(
    ' ' + '\n' +
    'SCSS Started'
  );


  return sass(paths_scss.scss_master, {
    // bundleExec: true,
    sourcemap: true
  })
  .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: SCSS Lint Error')}))
  .on('error', function (err) {
    console.error('Error!', err.message);
  })
  .pipe(isBuild ? plugins.util.noop() : plugins.sourcemaps.write())
  .pipe(isBuild ? plugins.util.noop() : plugins.mmq({log: mmq_log}))
  .pipe(plugins.plumber.stop())
  .pipe(gulp.dest(paths.prod_css))
  .pipe(isBuild ? plugins.cleanCSS() : plugins.util.noop())
  .pipe(isBuild ? plugins.header(banner, { pkg : pkg }) : plugins.util.noop())
  .pipe(isBuild ? plugins.footer(banner, { pkg : pkg }) : plugins.util.noop())
  .pipe(gulp.dest(paths.prod_css))
  .pipe(plugins.livereload(), cb);

});

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //



// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//   888888  .d8888b.
//     "88b d88P  Y88b
//      888 Y88b.
//      888  "Y888b.
//      888     "Y88b.
//      888       "888
//      88P Y88b  d88P
//      888  "Y8888P"
//    .d88P
//  .d88P"
// 888P"
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //

var paths_js = {

  js: [
    paths.dev_js + '*.js'
  ]

};


gulp.task('js', function (cb) {

  console.log(
    ' ' + '\n' +
    'Javascript Started'
  );

  return gulp.src(paths_js.js)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: JS Error')}))
    .pipe(isBuild ? plugins.util.noop() : plugins.cached('js-cache'))
    .pipe(isBuild ? plugins.util.noop() : plugins.remember('js-cache'))
    .pipe(isBuild ? plugins.util.noop() : plugins.print())
    .pipe(isBuild ? plugins.util.noop() : plugins.sourcemaps.init())
    .pipe(isBuild ? plugins.concat('zensation.min.js') : plugins.concatSourcemap('zensation.min.js'))
    .pipe(isBuild ? plugins.util.noop() : plugins.sourcemaps.write())
    .pipe(isBuild ? plugins.uglify({
      compress: {
        drop_console: true
      }
    }) : plugins.util.noop())
    .pipe(isBuild ? plugins.header(banner, { pkg : pkg }) : plugins.util.noop())
    .pipe(isBuild ? plugins.footer(banner, { pkg : pkg }) : plugins.util.noop())
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_js))
    .pipe(plugins.livereload(), cb);
});





// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //



// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 8888888 888b     d888  .d8888b.
//   888   8888b   d8888 d88P  Y88b
//   888   88888b.d88888 888    888
//   888   888Y88888P888 888
//   888   888 Y888P 888 888  88888
//   888   888  Y8P  888 888    888
//   888   888   "   888 Y88b  d88P
// 8888888 888       888  "Y8888P88
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //



var paths_imgs = {

  img: [
    paths.dev_img + '*.*',
    paths.dev_img + '**/*.*',
  ]

};


gulp.task('webp', function (cb) {

  console.log(
    ' ' + '\n' +
    'WEBP Started'
  );

  return gulp.src(paths_imgs.img)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: WEBP Error')}))
    .pipe(isBuild ? plugins.util.noop() : plugins.newer(paths.prod_img))
    .pipe(isBuild ? plugins.util.noop() : plugins.print())
    .pipe(plugins.webp({
      quality: 80
    }))
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_img))
    .pipe(plugins.livereload(), cb);

});


gulp.task('img', function (cb) {

  console.log(
    ' ' + '\n' +
    'IMG Started'
  );

  return gulp.src(paths_imgs.img)
    .pipe(isBuild ? plugins.util.noop() : plugins.newer(paths.prod_img))
    .pipe(plugins.imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant({quality: '65-80', speed: 4})]
    }))
    .pipe(gulp.dest(paths.prod_img))
    .pipe(plugins.livereload(), cb);

});


// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //




// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 88888888888 Y88b   d88P 8888888b.  8888888888
//     888      Y88b d88P  888   Y88b 888
//     888       Y88o88P   888    888 888
//     888        Y888P    888   d88P 8888888
//     888         888     8888888P"  888
//     888         888     888        888
//     888         888     888        888
//     888         888     888        8888888888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


var paths_type = {

  fonts: [
    paths.dev_fonts + '*.*',
    paths.dev_fonts + '**/*.*',
  ]

};



gulp.task('type', function (cb) {

  console.log(
    ' ' + '\n' +
    'Type Started'
  );

  return gulp.src(paths_type.fonts)
    .pipe(gulp.dest(paths.prod_fonts))
    .pipe(plugins.livereload(), cb);

});



// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //



// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 888       888        d8888 88888888888 .d8888b.  888    888
// 888   o   888       d88888     888    d88P  Y88b 888    888
// 888  d8b  888      d88P888     888    888    888 888    888
// 888 d888b 888     d88P 888     888    888        8888888888
// 888d88888b888    d88P  888     888    888        888    888
// 88888P Y88888   d88P   888     888    888    888 888    888
// 8888P   Y8888  d8888888888     888    Y88b  d88P 888    888
// 888P     Y888 d88P     888     888     "Y8888P"  888    888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


gulp.task('watch', function () {


  plugins.livereload.listen();


  console.log(
    ' ' + '\n' +
    '     [ black-marked ]' + '\n' +
    ' '
  );


  var watch_js = gulp.watch(paths_js.js, [
    'js'
  ]);
  watch_js.on('change', function (event) {
    if (event.type === 'deleted') {
      delete plugins.cached.caches['js-cache'];
      plugins.remember.forgetAll('js-cache');
    }
  });


  var watch_html = gulp.watch(paths_html.html, [
    'html'
  ]);
  watch_html.on('change', function (event) {
    if (event.type === 'deleted') {
      delete plugins.cached.caches['html-cache'];
    }
  });


  gulp.watch(paths_scss.scss_watch, ['scss']);
  gulp.watch(paths_imgs.img, ['img']);
  // gulp.watch(pathsCONFIG.config, ['config']);


});

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //








// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//  .d8888b.   .d88888b.  888b    888 8888888888 8888888 .d8888b.
// d88P  Y88b d88P" "Y88b 8888b   888 888          888  d88P  Y88b
// 888    888 888     888 88888b  888 888          888  888    888
// 888        888     888 888Y88b 888 8888888      888  888
// 888        888     888 888 Y88b888 888          888  888  88888
// 888    888 888     888 888  Y88888 888          888  888    888
// Y88b  d88P Y88b. .d88P 888   Y8888 888          888  Y88b  d88P
//  "Y8888P"   "Y88888P"  888    Y888 888        8888888 "Y8888P88
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


var paths_config = {

  config: [
    paths.dev_config + '.*',
    paths.dev_config + '*.*',
    paths.dev_config + '**/*.*',
  ]

};



gulp.task('config', function (cb) {

  console.log(
    ' ' + '\n' +
    'Config Started'
  );

  return gulp.src(paths_config.config)
    .pipe(gulp.dest(paths.prod_base))
    .pipe(plugins.livereload(), cb);

});


// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //










// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 888    888 888     888 888b     d888        d8888 888b    888  .d8888b.
// 888    888 888     888 8888b   d8888       d88888 8888b   888 d88P  Y88b
// 888    888 888     888 88888b.d88888      d88P888 88888b  888 Y88b.
// 8888888888 888     888 888Y88888P888     d88P 888 888Y88b 888  "Y888b.
// 888    888 888     888 888 Y888P 888    d88P  888 888 Y88b888     "Y88b.
// 888    888 888     888 888  Y8P  888   d88P   888 888  Y88888       "888
// 888    888 Y88b. .d88P 888   "   888  d8888888888 888   Y8888 Y88b  d88P
// 888    888  "Y88888P"  888       888 d88P     888 888    Y888  "Y8888P"
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


gulp.task('humans', function (cb) {
  gulp.src(paths.prod_base + 'index.html')
    .pipe(plugins.humans({
      team: 'Mark Schwanethal (@black_marked on Twitter) black-marked.com',
      site: [
        'Standards: HTML5, CSS3',
        'Components: modernizr.css',
        'Software: Sublime text 3'
      ],
      note: 'Built by Mark Schwanethal - black-marked.com.'
    }))
    .pipe(gulp.dest(paths.prod_base), cb);
});



// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //









// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//  .d8888b.   .d88888b.  888b     d888 8888888b.  888      8888888888 88888888888 8888888888
// d88P  Y88b d88P" "Y88b 8888b   d8888 888   Y88b 888      888            888     888
// 888    888 888     888 88888b.d88888 888    888 888      888            888     888
// 888        888     888 888Y88888P888 888   d88P 888      8888888        888     8888888
// 888        888     888 888 Y888P 888 8888888P"  888      888            888     888
// 888    888 888     888 888  Y8P  888 888        888      888            888     888
// Y88b  d88P Y88b. .d88P 888   "   888 888        888      888            888     888
//  "Y8888P"   "Y88888P"  888       888 888        88888888 8888888888     888     8888888888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


gulp.task('complete', ['humans'], function (cb) {
  return gulp.src('*.js', {read: false})
    .pipe(plugins.notify('Task: ' + action + ' complete'), cb);
});


// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //











// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 8888888b.  8888888888 888      8888888888        d8888  .d8888b.  8888888888
// 888   Y88b 888        888      888              d88888 d88P  Y88b 888
// 888    888 888        888      888             d88P888 Y88b.      888
// 888   d88P 8888888    888      8888888        d88P 888  "Y888b.   8888888
// 8888888P"  888        888      888           d88P  888     "Y88b. 888
// 888 T88b   888        888      888          d88P   888       "888 888
// 888  T88b  888        888      888         d8888888888 Y88b  d88P 888
// 888   T88b 8888888888 88888888 8888888888 d88P     888  "Y8888P"  8888888888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //




gulp.task('confirm', function (cb) {

  yesno.ask('Are you sure you want to perform a release? You should be in a release branch! Y/N', true, function (ok) {

    if (ok) {
      console.log(
        ' ' + '\n' +
        ' Release Started'
      );
      cb();
    } else {
      process.exit();
    }
    return;
  });

});


var versionOption = 'version not set';


// Bumping the version once confirmed
gulp.task('prompt', ['confirm'], function (cb) {
  return gulp.src('*')
    .pipe(plugins.prompt.prompt({
      type: 'checkbox',
      name: 'bump',
      message: 'What type of release would you like to do?',
      choices: ['patch', 'minor', 'major']
    }, function (res) {
      if (res.bump == 'minor' ) {
        versionOption = 'minor';
      } else if ( res.bump == 'major' ) {
        versionOption = 'major';
      } else if ( res.bump == 'patch' ) {
        versionOption = 'patch';
      } else {
        process.exit();
      }
    }));
});



// Bumping the version one confirmed
gulp.task('bump', ['prompt'], function (cb) {
  return gulp.src(['./package.json', './npm-shrinkwrap.json'])
    .pipe(plugins.bump({type: versionOption}))
    .pipe(gulp.dest('./'), cb);
});






// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //










// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//  .d8888b.  8888888888 .d88888b.  888     888 8888888888 888b    888  .d8888b.  8888888888
// d88P  Y88b 888       d88P" "Y88b 888     888 888        8888b   888 d88P  Y88b 888
// Y88b.      888       888     888 888     888 888        88888b  888 888    888 888
//  "Y888b.   8888888   888     888 888     888 8888888    888Y88b 888 888        8888888
//     "Y88b. 888       888     888 888     888 888        888 Y88b888 888        888
//       "888 888       888 Y8b 888 888     888 888        888  Y88888 888    888 888
// Y88b  d88P 888       Y88b.Y8b88P Y88b. .d88P 888        888   Y8888 Y88b  d88P 888
//  "Y8888P"  8888888888 "Y888888"   "Y88888P"  8888888888 888    Y888  "Y8888P"  8888888888
//                             Y8b
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //




gulp.task('dev', function (cb) {
  sequence('setup', tasks, 'watch', cb);
});

gulp.task('build', function (cb) {
  sequence('setup', tasks, 'complete', cb);
});

gulp.task('release', function (cb) {
  sequence('setup', tasks, 'complete', cb);
});


gulp.task('default', [ action ]);

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
